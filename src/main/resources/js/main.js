$(document).ready(()=>{
    setTimeout(()=>{
        $("#notice").hide();
    }, 2000);

    var $root = $(".root");
    if ($root.length){
        var id = parseInt($root.attr('id'),10) || 0;
        if (id>0){
            checkTask(id);
        }
    }
});

function checkTask(id) {
    $.get('/ax_task_check', {id:id}, result => {
        if (!result) return;
        if (result.status){
            console.log("Done", result);
            var $notice = $("#notice");
            $notice.html(`URL ${result.url} is parsed successfully!`);
            $notice.show();
            setTimeout(()=>{
                $("#notice").hide();
            }, 30000);
        }
        else{
            // console.log("checked", result);
            setTimeout(checkTask.bind(null,id), 5000);
        }
    });
}

function nextItems(q, sort='') {
    var offset = $('.items-holder').children().length;
    $.get('/ax_search', {q:q, offset:offset, sort:sort}, result => {
        var top = window.pageYOffset;
        var $btn = $("#next");
        $btn.button('loading');
        var html = result.reduce((acc, item)=>{
            return acc+`<div class="item">
<div class="title">
    <a href="${item.url}" target="_blank">${item.title}</a>
</div>
<div class="link">
    <a href="${item.url}" target="_blank">${item.url}</a>
</div>
    <div class="text">${item.content}</div>
</div>`;
        }, '');

        $('.items-holder').append(html);
        $btn.button('reset');
        window.scrollTo(0,top);
    })

}

function disableInputs(disabled=true) {
    // var $fields = $("select,input");
    var $btn = $("#start");
    if (disabled) {
        // $fields.attr("disabled", "disabled");
        $btn.button('loading');
    }
    else{
        // $fields.removeAttributeNode("disabled");
        $btn.button('reset');
    }
}