package com.userfoenix;

import com.userfoenix.services.crawler.Crawler;
import com.userfoenix.services.lucene.SearchEngineIndexer;
import com.userfoenix.services.lucene.SearchEngineSearcher;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configurable
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
@EnableAsync
public class GoogleFakeApplication implements AsyncConfigurer {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value(value = "${lucene.dir.path}")
	private String indexPath;

	@Bean(name = "indexReader")
	public IndexSearcher getIndexReader() throws IOException {
		IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(indexPath)));
		return new IndexSearcher(reader);
	}

	@Bean(name = "indexWriter")
	public IndexWriter getIndexWriter() throws IOException {
		Directory indexDirectory = FSDirectory.open(Paths.get(indexPath));
		StandardAnalyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		return new IndexWriter(indexDirectory, config);
	}

	@Bean(name = "searcher")
	public SearchEngineSearcher getSearchEngineSearcher() {
		return new SearchEngineSearcher();
	}

	@Bean(name = "indexer")
	public SearchEngineIndexer getSearchEngineIndexer(){
		return new SearchEngineIndexer();
	}

	@Bean(name = "crawler")
	public Crawler getCrawler(){
		return new Crawler(indexer);
	}

	@Autowired
	private SearchEngineIndexer indexer;

	public static void main(String[] args) {
		SpringApplication.run(GoogleFakeApplication.class, args);
	}

	@Override
	public Executor getAsyncExecutor() {
		return Executors.newSingleThreadExecutor();
	}

	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return null;
	}
}
