package com.userfoenix.controllers;

import com.userfoenix.services.lucene.SearchEngineIndexer;
import com.userfoenix.services.lucene.SearchEngineSearcher;
import com.userfoenix.domain.Page;
import com.userfoenix.services.crawler.*;
import com.userfoenix.validator.IndexerForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SearchEngineIndexer indexer;

    @Autowired
    private SearchEngineSearcher searcher;

    @Autowired
    private Crawler crawler;

    @Value(value = "${search.limit}")
    private int hits_limit;

    @Value(value = "${index.max_deep}")
    private int maxDeep;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index_form(IndexerForm indexerForm) {
        return "indexer";
    }

    @PostMapping("/index")
    public String indexer(@Valid IndexerForm indexerForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        indexerForm.setTaskId(1);
        logger.info(indexerForm.toString());
        if (bindingResult.hasErrors()) {
            return "indexer";
        }

        String url = indexerForm.getUrl();
        Integer deep = indexerForm.getDeep();
        Integer taskId = 0;

        try {
            CrawlerTask task = crawler.initTask(url, deep > maxDeep ? maxDeep : deep);
            crawler.runTask(task);
            taskId = task.getTaskId();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        logger.info("All done! Task=" + taskId);
        redirectAttributes.addFlashAttribute("taskId", taskId);
        return "redirect:/";
    }

    @RequestMapping(value = "/search")
    public String search_results(@RequestParam(value = "q", defaultValue = "", required = false) String q, @RequestParam(value = "sort", defaultValue = "") String sort, Model model) {
        int found = 0;
        if (q.length() > 0) {
            try {
                List<Page> items = searcher.search(q, hits_limit, 0, sort);
                found = items.size();
                model.addAttribute("items", items);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        model.addAttribute("q", q);
        model.addAttribute("found", found);
        model.addAttribute("sort", sort);
        model.addAttribute("limit", hits_limit);

        return "index";
    }

    @RequestMapping(value = "/ax_search", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    List<Page> ax_search(@RequestParam("q") String q, @RequestParam("offset") int offset, @RequestParam("sort") String sort) {
        if (q.length() == 0) {
            return new ArrayList<>();
        }
        return searcher.search(q, hits_limit, offset, sort);
    }

    @RequestMapping(value = "/ax_task_check", method = RequestMethod.GET, produces = "application/json")
    public
    @ResponseBody
    CrawlerTask ax_task_check(@RequestParam("id") Integer id) {
        return crawler.getTask(id);
    }
}