package com.userfoenix.services.crawler;

public interface CrawlerCallback {

    /**
     * Callback callable by the crawler after each url parsed
     *
     * @param text
     */
    public void process(String url, String title, String text);

    public void close();
}
