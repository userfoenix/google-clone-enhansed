package com.userfoenix.services.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class Crawler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private HashMap<String, CrawlerTask> tasks = new HashMap<>();
    private int threadCount;
    private CrawlerCallback indexer;

    public Crawler(){}

    public Crawler(CrawlerCallback indexer){
        this.indexer = indexer;
        this.threadCount = Runtime.getRuntime().availableProcessors();
    }

    public Crawler(CrawlerCallback indexer, int threadCount) {
        this.indexer = indexer;
        this.threadCount = threadCount;
    }

    public CrawlerTask getTask(String url) {
        return tasks.get(url);
    }

    public synchronized CrawlerTask getTask(int id) {
        for (Map.Entry<String, CrawlerTask> item : tasks.entrySet()){
            CrawlerTask task = item.getValue();
            if (task.getTaskId() == id){
                return task;
            }
        }
        return null;
    }

    public synchronized void setTaskStatus(String url, int status){
        tasks.get(url).setStatus(1);
    }

    public CrawlerTask initTask(String url, int maxdeep){
        CrawlerTask task = new CrawlerTask(url, maxdeep);
        tasks.put(url, task);
        return task;
    }

    @Async
    public void runTask(CrawlerTask task) throws MalformedURLException {
        logger.info("Start indexing");
        String url = task.getUrl();
        CrawlerAction.start(url, task.getMaxdeep(), threadCount, indexer);
        setTaskStatus(url, 1);
        logger.info("Stop indexing");
    }
}
