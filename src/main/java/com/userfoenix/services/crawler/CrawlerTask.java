package com.userfoenix.services.crawler;

public class CrawlerTask {
    private static int id = 0;
    private String url;
    private int maxdeep;
    private int status;
    private int taskId;

    CrawlerTask(String url, int maxdeep) {
        this.url = url;
        this.maxdeep = maxdeep;
        this.taskId = ++id;
        this.status = 0;
    }

    public String getUrl() {
        return url;
    }

    public int getStatus() {
        return status;
    }

    public int getMaxdeep() {
        return maxdeep;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTaskId() {
        return taskId;
    }
}