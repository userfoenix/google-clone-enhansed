package com.userfoenix.services.lucene;

import java.io.IOException;

import com.userfoenix.services.crawler.CrawlerCallback;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchEngineIndexer implements CrawlerCallback {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IndexWriter indexWriter;


    public SearchEngineIndexer(){
    }

    @Override
    public void close() {
        try {
            indexWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void process(String url, String title, String text) {
        // Good idea to append content into the text file(s) and process after, to increase crawling speed
        // After crawler will finished give folder with files to Lucene indexer
        try {
            Document doc = new Document();
            doc.add(new TextField("content", text, Field.Store.YES));
            doc.add(new StringField("title", title, Field.Store.YES));
            doc.add(new SortedDocValuesField("title", new BytesRef(title)));
            doc.add(new StringField("url", url, Field.Store.YES));
            indexWriter.addDocument(doc);
            logger.info("Parsed: " + url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}